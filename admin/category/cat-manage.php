<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href='../../public/styles/cat-manage.css' >
    <link rel="stylesheet" href='../../public/bootstrap.min.css' >
    <title>Document</title>
    <style>
        #ok{
            direction: rtl;
            font-size: 20px;
            color: seagreen;
        }
        #empty{
            font-size: 20px;
            color: darkslategrey;
        }
        #err{
            font-size: 20px;
            color: crimson;
        }
    </style>
</head>
<body >
    <div  class="container">
        <div class="form-group mt-5 " id="form">
            <form method="post" action="" >
                <input type="text"  name="catName" id="inp1" placeholder="نام دسته" class="form-control ">
                <button type="submit" id="btn" class="btn btn-primary mt-3" name="btn">درج دسته</button>
            </form>
        </div>
        <div id="php-tag">
            <?php
            include "../../funcs/connect.php";
            include "../../funcs/funcs.php";
            if(isset($_POST['btn'])) {
                if (empty($_POST['catName'])) {
                    echo "<div id='empty' >".$_POST['catName']."کادر خالی می باشد"."</div>";
                } else {
                    $str = xss($_POST['catName']);
                    $str2 = toUpper($str);
                    $sql = "INSERT INTO `cat` (`catid`, `name`) VALUES (NULL, ?);";
                    $result = $connect->prepare($sql);
                    $result->bindValue(1, $str2);
                    $query = $result->execute();
                    if ($query) {
                        echo "<div id='ok' >" . $_POST['catName'] . " با موفقیت ثبت شد" . "</div>";
                    } else {
                        echo "<div id='err' >" . " خطا در ثبت " . "</div>";
                    }

                }
            }
?>
            <div id="column" class="text-center container p-4">
            <div class="row mt-2 text-white ">
                <div class="col-1 mr-1 ml-1 " id="cat-id">شماره</div>
                <div class="col mr-1" id="cat-name">نام دسته</div>
                <div class="col mr-1" id="rm-cat">حذف دسته</div>
                <div class="col mr-1" id="edit-cat">ویرایش دسته</div>
            </div>
                <?php
                $sql2 = "SELECT * FROM cat";
                $result2 = $connect->prepare($sql2);
                $query2 = $result2->execute();
                while ($rows = $result2->fetch(PDO::FETCH_ASSOC)) {
                    echo '
              
                    <div class="row mt-2 text-white">
                        <div class="col-1 mr-1 ml-1" id="cat-id">'.$rows["catid"].'</div>
                        <div class="col mr-1 text-white" id="cat-name">'.$rows["name"].'</div>
                        <div class="col mr-1" id="rm-cat"><a href="del-page.php?id='.$rows['catid'].' ">حذف دسته</a></div>
                        <div class="col mr-1" id="edit-cat"> <a href="up-page?id='.$rows['catid'].' ">ویرایش دسته</a> </div>
                    </div>';
                }
                if(isset($_GET['del'])){
                    echo "<div id='ok' >"." با موفقیت حذف شد" . "</div>";
                }
                if(isset($_GET['err'])){
                    echo "<div id='err' >" . " خطا در حذف " . "</div>";

                }
            ?>
        </div>

        </div>
    </div>
</body>
</html>